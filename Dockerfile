FROM ubuntu:zesty
MAINTAINER Luke Murphy "lukewm@riseup.net"

EXPOSE 8000

WORKDIR /oidcp/
COPY . /oidcp/

RUN apt update

RUN cat system-requirements.txt | xargs apt install -y

RUN ln -s /usr/bin/pip3 /usr/bin/pip
RUN ln -s /usr/bin/python3 /usr/bin/python

RUN pip install -I -r requirements/base.txt
RUN pip install -I -r requirements/production.txt
RUN pip install -e .
