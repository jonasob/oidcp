"""Celery tasks module."""

from urllib.parse import urlencode

import requests
from flask import current_app

from oidcp.app import celery


@celery.task
def send_async_email(msg):
    """Send an email asynchronously."""
    from flask import current_app

    current_app.mail.send(msg)


@celery.task
def store_async_fellow_id(email, session_id):
    """Retrieve the Fellow account record identifier for given email."""
    session_db = current_app.provider.sdb
    timeout = current_app.config['BACKCHANNEL_TIMEOUT']
    backchannel_id = current_app.config['BACKCHANNEL_ID']
    backchannel_pword = current_app.config['BACKCHANNEL_PWORD']
    fsfe_cd_uri = current_app.config['FSFE_CD_URL']
    auth = (backchannel_id, backchannel_pword)
    query = urlencode({'email': email})
    url = "{}/records/?{}".format(fsfe_cd_uri, query)

    try:
        response = requests.get(url, timeout=timeout, auth=auth)
        session = session_db[session_id]
        session['record'] = response.json[email]
        session_db[session_id] = session
    except Exception as err:
        msg = ("oidcp.tasks.store_async_fellow_id: "
               "unable to retrieve record identifier for '%s'")
        current_app.logging.error(msg, email)
