# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The PyOIDC provider module."""

import binascii
from datetime import datetime as dt

import jwt
from flask import Response, current_app, jsonify
from flask_restful import abort
from oic.oic.message import AuthorizationRequest
from oic.oic.provider import Provider as PYOIDCProvider
from oic.utils.authn.client import verify_client
from oic.utils.sdb import AuthnEvent

from oidcp.auth import requires_basic_auth


class Provider(PYOIDCProvider):
    """A Provider instance."""

    def get_auth(self):
        """The authentication method for this provider."""
        return self.authn_broker[0][0]

    def logout_endpoint(self, access_token):
        """Remove the session that corresponds to the given access_token."""
        if not self.validate_access_token(access_token):
            return abort(422, errors=dict(access_token="Invalid token"))

        sub = self.sdb[access_token]['sub']
        session_id = self.sdb.get_sids_by_sub(sub)[0]
        del self.sdb[session_id]

        return Response(status=200)

    @requires_basic_auth
    def introspection_endpoint(self, access_token):
        """Determine if an access_token is still valid."""
        return jsonify({'active': self.validate_access_token(access_token)})

    def validate_access_token(self, access_token):
        """Verify access_token and corresponding id_token are valid."""
        try:
            validator = self.sdb.token_factory["access_token"]
            is_active = validator.valid(access_token)
        except (binascii.Error, KeyError):
            return False

        client_id = self.sdb[access_token]['client_id']
        alg = self.cdb[client_id]['id_token_signed_response_alg']
        secret = self.cdb[client_id]['client_secret']
        id_token = self.sdb[access_token]['id_token']

        try:
            jwt.decode(id_token, secret, algorithms=alg, audience=client_id)
        except (jwt.exceptions.ExpiredSignatureError,
                jwt.exceptions.DecodeError) as err:
            msg = ("oidcp.provider.validate_access_token: "
                   'Failed to decode id_token. Saw "%s"')
            current_app.logging.error(msg, str(err))
            return False

        return is_active and not self.sdb.is_revoked(access_token)

    def create_session(self, query, username):
        """Generate token using the PyOIDC interface."""
        authz_request = AuthorizationRequest().from_dict(query)
        event = AuthnEvent(username, None)
        client_db = self.cdb[query['client_id']]
        return self.setup_session(authz_request, event, client_db)

    def userinfo_in_id_token_claims(self, session):
        """User related claims to be included in the id_token."""
        try:
            return dict(record=session['record'])
        except KeyError as err:
            msg = "No record identifier for user claims"
            abort(422, errors=dict(record=msg))

    def token_endpoint(self, request="", code=None, **kwargs):
        """The token endpoint handler."""
        try:
            event = self.sdb[code]['authn_event']
        except KeyError:
            msg = "No session corresponds to given code"
            abort(422, errors=dict(code=msg))

        # https://github.com/OpenIDC/pyoidc/issues/328
        now = int(dt.now().timestamp())
        code_factory = self.sdb.token_factory['code']
        ttl = event.authn_time + code_factory.lifetime
        if now >= ttl:
            msg = "Code has timed out"
            return abort(422, errors=dict(code=msg))

        return super().token_endpoint(request, **kwargs)


def configure_provider(app):
    """Configure a provider with the application configuration."""
    issuer = app.config['ISSUER']
    session_db = app.config['SESSION']
    client_db = app.config['CLIENT']
    authz_handler = app.config['AUTHZ']

    auth_broker = None
    user_info_store = None
    sym_key = ''

    verification_function = verify_client

    return Provider(
        issuer,
        session_db,
        client_db,
        auth_broker,
        user_info_store,
        authz_handler,
        verification_function,
        sym_key,
    )
