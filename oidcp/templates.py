# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Convenience functions for dealing with templates."""

from jinja2 import Environment, PackageLoader, select_autoescape


def get_template(template_name):
    """Load the login form template from source."""
    pkg_loader = PackageLoader('oidcp', 'templates')
    html = select_autoescape(['html'])
    env = Environment(loader=pkg_loader, autoescape=html)
    return env.get_template(template_name)
