# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""View request validation."""

from urllib.parse import parse_qsl

from flask_restful import abort
from webargs import ValidationError, fields, validate
from webargs.flaskparser import parser


@parser.error_handler
def handle_request_parsing_error(err):
    """Error handler for flask-restful X webargs compatibility."""
    abort(422, errors=err.messages)


def query_parser(query):
    """Parse a query string into a dictionary."""
    return dict(parse_qsl(query))


def auth_post_validator(reqargs):
    """Validate request payload for authorization endpoint."""
    login_type = reqargs['query']['login_type']
    email = reqargs.get("email", False)
    username = reqargs.get("username", False)
    password = reqargs.get("password", False)
    if login_type == "email":
        if not email or any((username, password)):
            msg = "Invalid parameters for login_type=email"
            raise ValidationError(dict(login_type=msg))
    if login_type == "username":
        if email or not all((username, password)):
            msg = "Invalid parameters for login_type=username"
            raise ValidationError(dict(login_type=msg))


auth_get_request_parser = {
    'response_type': fields.Str(required=True),
    'client_id': fields.Str(required=True),
    'redirect_uri': fields.Str(required=True),
    'scope': fields.Str(required=True),
    'login_type': fields.Str(
        required=True,
        validate=validate.OneOf(["username", "email"])
    ),
    'state': fields.Str(),
    'nonce': fields.Str()
}


auth_post_map = {
    'query': fields.Function(deserialize=query_parser, required=True),
    'username': fields.Str(),
    'password': fields.Str(),
    'email': fields.Email(validate=validate.Email)
}

token_post_map = {
    'grant_type': fields.Str(required=True),
    'client_id': fields.Str(required=True),
    'client_secret': fields.Str(required=True),
    'code': fields.Str(required=True),
    'redirect_uri': fields.Url(required=True)
}

introspect_post_map = {'access_token': fields.Str(required=True)}
logout_post_map = {'access_token': fields.Str(required=True)}
