# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The main application factory."""

import logging

from celery import Celery
from flasgger import Swagger
from flask import Flask
from flask_mail import Mail
from opbeat.contrib.celery import register_signal
from opbeat.contrib.flask import Opbeat

from oidcp.auth import configure_auth_broker
from oidcp.config import Base, Prod, Test
from oidcp.provider import configure_provider
from oidcp.views import configure_views
from swagger_docs.template import base_template

celery = Celery(__name__, broker=Base.CELERY_BROKER_URL)

logging.basicConfig(level=logging.ERROR)
logging.getLogger("oic").setLevel(logging.ERROR)

opbeat = Opbeat(
    organization_id=Base.OPBEAT_ORGANIZATION_ID,
    app_id=Base.OPBEAT_APP_ID,
    secret_token=Base.OPBEAT_SECRET_TOKEN,
    logging=True
)


def create_app(config=Prod, init_opbeat=True):
    """Application factory."""
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config)

    if config.ENV == Test.ENV:
        app = configure_mocked(app)

    if config.ENV == Prod.ENV and init_opbeat:
        opbeat.init_app(app)
        register_signal(opbeat.client)

    app.provider = configure_provider(app)
    app.mail = Mail(app)
    configure_views(app)
    configure_auth_broker(app.provider)
    Swagger(app, template=base_template)
    celery.conf.update(app.config)
    app.logging = logging

    return app


def configure_mocked(app):
    """Configure mocked session and connection interfaces."""
    from oidcp.session import SessionDB
    from tests.session import FakeRedisSessionDB
    from mockldap import MockLdap

    timeout = app.config['CODE_TIMEOUT']
    db = FakeRedisSessionDB()
    app.config['SESSION'] = SessionDB('', db=db, grant_expires_in=timeout)
    app.config['LDAP_CONN'] = MockLdap(dict([
        ("dc=fsfe,dc=org", {"objectClass": ["top", "domain"], "dc": ["fsfe"]}),
        ("ou=fellowship,dc=fsfe,dc=org", {
            "objectClass": ["organizationalUnit", "top"], "ou": ["fellowship"]
        }),
        ("uid=alice,ou=fellowship,dc=fsfe,dc=org", {
            "objectClass": ["top", "inetOrgPerson"],
            "cn": ["alice"],
            "sn": ["alice"], "uid": ["alice"],
            "mail": ["alice@fsfe.org"],
            "description": ["FC12345"],
            "userPassword": ["passw0rd"]
         })
    ]))
    return app
