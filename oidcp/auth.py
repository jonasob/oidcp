# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The LDAP authentication module."""

from base64 import b64decode
from binascii import Error
from functools import wraps
from urllib.parse import parse_qsl

from flask import current_app, request, url_for
from flask_restful import abort
from ldap import (INVALID_CREDENTIALS, NO_SUCH_OBJECT, SCOPE_ONELEVEL,
                  SCOPE_SUBTREE)
from oic.oic.message import AuthorizationRequest
from oic.utils.authn.authn_context import AuthnBroker
from oic.utils.authn.user import UserAuthnMethod
from oic.utils.http_util import Response as PyOIDCResponse

from oidcp.config import Prod
from oidcp.templates import get_template


def ldap_init():
    """Initialize the LDAP connection and configuration."""
    ldap = current_app.config['LDAP_CONN']
    if current_app.config['ENV'] == Prod.ENV:
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
    ldap_url = current_app.config['LDAP_URL']
    return ldap.initialize(ldap_url)


def configure_auth_broker(provider):
    """Configure authentication back-end for the provider."""
    authn_broker = AuthnBroker()
    authn_broker.add("LDAP", LDAPAuth(provider))
    provider.authn_broker = authn_broker


def ldap_get_username(email):
    """Retrieve the username of the LDAP account with given email."""
    ldap = ldap_init()
    base = current_app.config['LDAP_EMAIL_PATTERN']
    search = "(mail={})".format(email)

    try:
        result = ldap.search_s(base, SCOPE_ONELEVEL, search, attrlist=["uid"])
        attribute = result[0][1]
        return attribute['uid'][0]
    except (NO_SUCH_OBJECT, KeyError, IndexError, TypeError) as err:
        msg = ("oidcp.auth.LDAPAuth.ldap_account_exists: "
               "Failed to retrieve username from LDAP for email '%s'")
        current_app.logging.error(msg, email)


def requires_basic_auth(f):
    """Back-channel HTTP Basic authentication verifier."""
    @wraps(f)
    def decorated(*args, **kwargs):
        headers = request.headers
        identifier = current_app.config['BACKCHANNEL_ID']
        password = current_app.config['BACKCHANNEL_PWORD']
        try:
            decoded = str(b64decode(headers['Basic']), "utf-8")
            client_id, client_secret = decoded.split(":", 1)
            matching_client = client_id == identifier
            matching_password = client_secret == password
            if not all((matching_client, matching_password)):
                return abort(401)
        except (KeyError, Error, TypeError):
            return abort(401)
        return f(*args, **kwargs)
    return decorated


class LDAPAuth(UserAuthnMethod):
    def __init__(self, *args, **kwargs):
        self.templates = {
            'username': 'uname-pword-login.html',
            'email': 'email-login.html'
        }
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        """Respond with the rendered login page."""
        request = dict(parse_qsl(kwargs['query']))
        requested_prompt = request['login_type']
        template_name = self.templates[requested_prompt]
        endpoint = url_for("auth", _external=True)
        context = dict(endpoint=endpoint, query=kwargs['query'])
        template = get_template(template_name)
        rendered_template = template.render(context)
        return PyOIDCResponse(rendered_template)

    def verify(self, username, password, query):
        """Validate the End-User credentials via LDAP."""
        from oidcp.tasks import store_async_fellow_id

        ldap = ldap_init()
        pattern = current_app.config['LDAP_UNAME_PATTERN']

        try:
            base = pattern.format(username)
            ldap.simple_bind_s(base, password)
        except (INVALID_CREDENTIALS, TypeError):
            return PyOIDCResponse(status='401 Unauthorized')

        session_id = current_app.provider.create_session(query, username)

        try:
            result = ldap.search_s(base, SCOPE_SUBTREE, attrlist=["mail"])
            entry = dict(result).values()
            ldap_email = list(entry)[0]['mail'][0]
            store_async_fellow_id.apply_async(args=(ldap_email, session_id))
        except (NO_SUCH_OBJECT, KeyError, IndexError, TypeError) as err:
            msg = ("oidcp.auth.LDAPAuth.verify: "
                   "unable to retrieve LDAP account email for username: '%s'")
            current_app.logging.error(msg, username)

        authz_request = AuthorizationRequest().from_dict(query)
        return self.srv.authz_part2(username, authz_request, session_id)
