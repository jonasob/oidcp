# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The Application settings."""

from os import environ, pardir
from os.path import abspath, dirname, join

import ldap
from oic.utils.authz import AuthzHandling
from oic.utils.client_management import pack_redirect_uri

from oidcp.session import SessionDB


class Base():
    """The base configuration."""
    DEBUG = False
    JSON_AS_ASCII = False

    APP_DIR = abspath(dirname(__file__))
    PROJECT_ROOT = abspath(join(APP_DIR, pardir))
    SWAGGER_DIR = abspath(join(PROJECT_ROOT, "swagger_docs"))

    AUTHZ = AuthzHandling()
    FLOW = "code"
    SCOPE = "openid"
    ISSUER = "fsfe"
    CODE_TIMEOUT = int(environ['CODE_TIMEOUT'])
    FRONTEND_URL = environ['FRONTEND_URL']
    FSFE_CD_URL = environ['FSFE_CD_URL']
    FRONTEND_ID = environ['FRONTEND_ID']
    SHARED_JWT_SECRET = environ['SHARED_JWT_SECRET']
    FRONTEND_SALT = environ['FRONTEND_SALT']
    TOKEN_ALGORITHM = environ['TOKEN_ALGORITHM']
    CLIENT = {
        FRONTEND_ID: {
            "response_types_supported": "code",
            "redirect_uris": pack_redirect_uri([FRONTEND_URL]),
            "client_salt": FRONTEND_SALT,
            "client_secret": SHARED_JWT_SECRET,
            "token_endpoint_auth_method": "client_secret_post",
            "id_token_signed_response_alg": TOKEN_ALGORITHM
        }
    }

    LDAP_URL = environ['LDAP_URL']
    LDAP_UNAME_PATTERN = environ['LDAP_UNAME_PATTERN']
    LDAP_EMAIL_PATTERN = environ['LDAP_EMAIL_PATTERN']

    SWAGGER = {
        'uiversion': environ['SWAGGER_UI'],
        'specs_route': '/api/'
    }

    BACKCHANNEL_ID = environ['BACKCHANNEL_ID']
    BACKCHANNEL_PWORD = environ['BACKCHANNEL_PWORD']
    BACKCHANNEL_TIMEOUT = int(environ['BACKCHANNEL_TIMEOUT'])

    SESSION_EMAIL_SUBJECT = "Requested session for ams.fsfe.org"

    MAIL_DEFAULT_SENDER = environ['MAIL_DEFAULT_SENDER']
    MAIL_SERVER = environ['MAIL_SERVER']

    REDIS_HOST = environ['REDIS_HOST']
    REDIS_PORT = environ['REDIS_PORT']
    REDIS_SESSION_DB = int(environ['REDIS_SESSION_DB'])

    CELERY_BROKER_URL = environ['CELERY_BROKER_URL']
    CELERY_RESULT_BACKEND = environ['CELERY_RESULT_BACKEND']

    OPBEAT_ORGANIZATION_ID = environ['OPBEAT_ORGANIZATION_ID']
    OPBEAT_APP_ID = environ['OPBEAT_APP_ID']
    OPBEAT_SECRET_TOKEN = environ['OPBEAT_SECRET_TOKEN']


class Prod(Base):
    """The production configuration."""
    from oidcp.session import RedisSessionDB
    from redis import Redis

    ENV = "prod"
    LDAP_CONN = ldap
    REDIS = RedisSessionDB(Redis(
        host=Base.REDIS_HOST,
        port=Base.REDIS_PORT,
        db=Base.REDIS_SESSION_DB
    ))
    SESSION = SessionDB(
        "",
        db=REDIS,
        grant_expires_in=Base.CODE_TIMEOUT
    )


class Test(Base):
    """The test configuration."""
    ENV = "test"
    DEBUG = True
    MAIL_SUPPRESS_SEND = True
    CELERY_ALWAYS_EAGER = True
