# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.


"""View routing."""

import json
from os.path import join
from urllib.parse import urlencode

from flasgger.utils import swag_from
from flask import Response as FlaskResponse
from flask import current_app, jsonify, request
from flask_mail import Message
from flask_restful import Api, Resource, abort
from oic.utils.http_util import Response as PyOIDCResponse
from oic.utils.http_util import extract_from_request
from webargs.flaskparser import use_kwargs

from oidcp.auth import ldap_get_username
from oidcp.config import Base
from oidcp.exceptions import MissingImplementation
from oidcp.templates import get_template
from oidcp.validators import (auth_get_request_parser, auth_post_map,
                              auth_post_validator, introspect_post_map,
                              logout_post_map, token_post_map)


class CustomResource(Resource):
    """A flask-restful Resource subclass.

    This class provided a single new method, self.handler,
    which does the necessary conversion between the custom
    Response class that PyOIDC returns from its interface.
    We only want to return Flask style responses.
    """

    def handler(self, response):
        """Custom response handler for non-Flask responses."""
        if not isinstance(response, PyOIDCResponse):
            return response
        status = int(response.status.split(" ")[0])
        content_type = dict(response.headers)['Content-type']
        if content_type == "application/json":
            data = json.loads(response.message)
            if "error" in data:
                if "error_description" in data:
                    data = dict(errors=dict(oic=data['error_description']))
                else:
                    data = dict(errors=dict(oic=data['error']))
            return data, status, response.headers
        elif content_type == 'text/html':
            data = response.message
            return FlaskResponse(data, status, response.headers)
        else:
            raise MissingImplementation


def configure_views(app):
    """Attach all views to the application API."""
    api = Api(prefix="/api")
    api.add_resource(AuthResource, "/auth/", endpoint="auth")
    api.add_resource(TokenResource, "/token/", endpoint="token")
    api.add_resource(IntrospectResource, "/introspect/", endpoint="introspect")
    api.add_resource(StatusResource, "/status/", endpoint="status")
    api.add_resource(LogoutResource, "/logout/", endpoint="logout")
    api.init_app(app)


def swagger_spec(spec, **kwargs):
    """Arrange paths for importing Swagger specification."""
    folder = spec.split("_")[0]
    root = Base.PROJECT_ROOT
    path = join(root, "swagger_docs", folder, spec)
    return swag_from(path, **kwargs)


class AuthResource(CustomResource):
    """The Authorization Resource."""

    @swagger_spec("auth_get.yml", methods=["GET"])
    @use_kwargs(auth_get_request_parser)
    def get(self, **kwargs):
        """HTTP GET request handler."""
        environ = extract_from_request(request.environ)
        response = current_app.provider.authorization_endpoint(**environ)
        return self.handler(response)

    @swagger_spec("auth_post.yml", methods=['POST'])
    @use_kwargs(auth_post_map, validate=auth_post_validator)
    def post(self, query, username=None, password=None, email=None):
        """HTTP POST request handler."""
        if query['login_type'] == "username":
            ldap_authn = current_app.provider.get_auth()
            response = ldap_authn.verify(username, password, query)
            return self.handler(response)
        return self.email_account_auth(email, query)

    def email_account_auth(self, email, query):
        """Deliver the code for exchange via email."""
        from oidcp.tasks import send_async_email, store_async_fellow_id

        username = ldap_get_username(email)
        if not username:
            msg = "No account corresponds to given email"
            return abort(422, errors=dict(email=msg))

        session_id = current_app.provider.create_session(query, username)
        code = current_app.provider.sdb[session_id]['code']
        url = "{}/?code={}".format(query['redirect_uri'], code)

        template = get_template("session-token.html")
        body = template.render(dict(url=url))
        subject = current_app.config['SESSION_EMAIL_SUBJECT']
        msg = Message(recipients=[email], body=body, subject=subject)

        send_async_email(msg)

        store_async_fellow_id.apply_async(args=(email, session_id))

        return FlaskResponse(status=202)


class TokenResource(CustomResource):
    """The Token Resource."""

    @swagger_spec("token_post.yml")
    @use_kwargs(token_post_map)
    def post(self, **kwargs):
        """HTTP POST request handler."""
        request_string = urlencode(request.form)
        response = current_app.provider.token_endpoint(
            request=request_string, **kwargs
        )
        return self.handler(response)


class IntrospectResource(CustomResource):
    """The Introspection Resource."""

    @swagger_spec("introspect_post.yml")
    @use_kwargs(introspect_post_map)
    def post(self, access_token):
        """HTTP POST request handler."""
        return current_app.provider.introspection_endpoint(access_token)


class StatusResource(CustomResource):
    """The Status Resource."""

    @swagger_spec("status_get.yml")
    def get(self):
        """HTTP GET request handler."""
        return jsonify({'stádas': 'Ta me réidh!'})


class LogoutResource(CustomResource):
    """The Logout Resource."""

    @swagger_spec("logout_post.yml")
    @use_kwargs(logout_post_map)
    def post(self, access_token):
        """HTTP POST request handler."""
        return current_app.provider.logout_endpoint(access_token)
