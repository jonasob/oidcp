SPHINX_OPTS   =
SPHINXBUILD   = sphinx-build
SPHINXABUILD  = sphinx-autobuild
SOURCEDIR     = oidcp
DOCSDIR       = sphinx_docs
BUILDDIR      = sphinx_docs/_build
PROJECT_ROOT  = .

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  lint             to check the source for stylistic errors."
	@echo "  clean            to remove all cache files."
	@echo "  install          to install the project python dependencies."
	@echo "  system_install   to install the project system dependencies."
	@echo "  test             to run the tests."
	@echo "  isort            to check the source for disorganised imports."
	@echo "  ci               to imitate a Gitlab CI build run."
	@echo "  coverage         to check the code coverage for the source. "
	@echo "  devserver        to serve the application for development."
	@echo "  prodserver       to serve the application for production."
	@echo "  upgrade          to upgrade the python dependencies."
	@echo "  tags             to generate tags for the project."
	@echo "  celery           to fire up a celery worker."
	@echo "  docs_clean       to clean the documentation build directory."
	@echo "  html             to make the html documentation."
	@echo "  livehtml         to make the html documentation with live reload support."
.PHONY: help

lint:
	@pylama oidcp/ tests/
.PHONY: lint

clean:
	@find . -name "__pycache__" | xargs rm -rf
.PHONY: clean

install:
	@pip install -U -e . \
	-r requirements/base.txt \
	-r requirements/development.txt \
	-r requirements/quality.txt \
	-r requirements/documentation.txt
.PHONY: install

system_install:
	@cat system-requirements.txt | xargs sudo apt install -y
.PHONY: system_install

test:
	@py.test -v tests/
.PHONY: test

isort: clean
	@find oidcp -name "*.py" | xargs isort -c -q
.PHONY: isort

coverage:
	@py.test --cov=oidcp tests
.PHONY: coverage

ci: lint isort test
.PHONY: ci

devserver:
	@flask run
.PHONY: devserver

prodserver:
	@gunicorn --bind 0.0.0.0:8000 wsgi:app
.PHONY: prodserver

REQS_DIR=$(PROJECT_ROOT)/requirements
BASE_DEPS:=$(REQS_DIR)/base.txt
ADMIN_DEPS:=$(REQS_DIR)/administration.txt
DEV_DEPS:=$(REQS_DIR)/development.txt
DOC_DEPS:=$(REQS_DIR)/documentation.txt
PROD_DEPS:=$(REQS_DIR)/production.txt
QA_DEPS:=$(REQS_DIR)/quality.txt
ALL_REQS:=$(BASE_DEPS) $(ADMIN_DEPS) $(DEV_DEPS) $(DOC_DEPS) $(PROD_DEPS) $(QA_DEPS)
reqs: $(ALL_REQS)
upgrade:
	$(RM) $(ALL_REQS)
	$(MAKE) reqs PIP_COMPILE_ARGS=--rebuild
.PHONY: upgrade

$(REQS_DIR)/%.txt: PIP_COMPILE_ARGS?=
$(REQS_DIR)/%.txt: $(REQS_DIR)/%.in
	pip-compile --no-header $(PIP_COMPILE_ARGS) --output-file "$@.tmp" "$<" >/tmp/pip-compile.out.tmp || { \
	  ret=$$?; echo "pip-compile failed:" >&2; cat /tmp/pip-compile.out.tmp >&2; \
	  $(RM) "$@.tmp" /tmp/pip-compile.out.tmp; \
	  exit $$ret; }
	@sed -n '1,10 s/# Depends on/-r/; s/\.in/.txt/p' "$<" > "$@"
	@cat "$@.tmp" >> "$@"
	@$(RM) "$@.tmp" /tmp/pip-compile.out.tmp

tags:
	@rm -rf tags
	@ctags -R oidcp/ tests/
.PHONY: tags

celery:
	@celery worker -A worker.celery
.PHONY: celery

docs_clean:
	rm -rf $(BUILDDIR)/*
.PHONY: docs_clean

html: docs_clean
	$(SPHINXBUILD) -b html $(DOCSDIR) $(BUILDDIR)/html $(SPHINX_OPTS)
.PHONY: html

livehtml: docs_clean
	$(SPHINXABUILD) -b html $(DOCSDIR) $(BUILDDIR)/html $(SPHINX_OPTS)
.PHONY: livehtml

docker_build:
	@docker build -t registry.gitlab.com/lukewm/oidcp/image .
.PHONY: docker_build

docker_push:
	@docker push registry.gitlab.com/lukewm/oidcp/image
.PHONY: docker_push
