[![build status](https://gitlab.com/lukewm/oidcp/badges/master/build.svg)](https://gitlab.com/lukewm/oidcp/commits/master)
[![coverage report](https://gitlab.com/lukewm/oidcp/badges/master/coverage.svg)](https://gitlab.com/lukewm/oidcp/commits/master)
[![Documentation Status](https://readthedocs.org/projects/oidcp/badge/?version=latest)](http://oidcp.readthedocs.io/en/latest/?badge=latest)

# oidcp

The [OpenID Connect] provider behind the account management system.

It's a [Python 3.5.X] project.

Please refer to [the documentation] for more information.

[OpenID Connect]:  http://openid.net/
[the documentation]: http://oidcp.readthedocs.io/en/latest/
[Python 3.5.X]: https://www.python.org/downloads/release/python-353/
