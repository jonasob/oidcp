"""Base template information for Swagger documentation."""

base_template = {
    "info": {
        "title": "FSFE OpenID Connect Provider",
        "description": ("An OpenID Connect provider created and maintained by"
                        " Free Software Foundation Europe for the identity"
                        " management of the account management system."),
        "consumes": [
            "application/json",
            "application/x-www-form-urlencoded",
        ],
        "produces": [
            "application/json",
            "text/html",
        ],
        "contact": {
            "responsibleDeveloper": "ams-hackers@q.fsfe.org",
            "email": "ams-hackers@q.fsfe.org",
            "version": "0.0.1"
        },
        "basePath": "/api",
    }
}
