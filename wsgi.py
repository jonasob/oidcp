"""Bootstrap the application for web serving."""

import os
from distutils.util import strtobool

from oidcp.app import create_app
from oidcp.config import Prod, Test

if strtobool(os.environ.get('DEBUG', False)):
    app = create_app(config=Test)
else:
    app = create_app(config=Prod)
