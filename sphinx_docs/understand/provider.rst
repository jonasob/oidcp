Knowledge Guide
---------------

What is an OpenID Connect Provider?
===================================

An OpenID connect provider is a web service which provides authentication,
implemented using the `OpenID Connect specification`_. Authentication is the
process of proving your identity to a web service, typically by entering your
username and password.

.. _OpenID Connect Specification: http://openid.net/specs/openid-connect-core-1_0.html

OpenID Connect is an extension of the `OAuth 2.0 specification`_. The main
purpose of the OAuth 2.0 specification is to provide a standardized way to
authorize a user of a web service. Authorization is the process of determining
which permissions a user has, which are required to access resources of a web
service. Authorization occurs after authentication - the web service knows who
you are, but what are you allowed to do?

.. _OAuth 2.0 specification: https://tools.ietf.org/html/rfc6749

When a user authenticates with a provider, they permit the provider to issue
proof of their identity (the successful authentication) and proof of their
permissions (the successful authorization) to a client who may then use it
in order to access protected web resources.

Please review `the benefits`_ of OpenID Connect to better understand why the
specification is useful.

.. _the benefits: http://openid.net/get-an-openid/individuals/


Web API Documentation
=====================

A `Swagger UI`_ documentation portal is currently available if are running the
provider locally, this documentation is available at http://127.0.0.1:5500/apidocs/.
This interface shows all available end-points provided along with the parameters they
accept.

.. _Swagger UI: http://swagger.io/


BPMN Diagrams
=============

`BPMN`_ diagrams are provided to understand what role the provider plays in the
functioning of the account management system. Please review the following
scenarios:

.. _BPMN: https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation

  * `The end-user logs in with username and password authentication`_
  * `The end-user logs in with email ticket authentication`_
  * `The end-user requests their fellow account information`_
  * `The end-user requests a password reset`_
  * `The end-user registers a new account`_

.. _The end-user logs in with username and password authentication: ../_static/bpmn/username_password_login.png
.. _The end-user logs in with email ticket authentication: ../_static/bpmn/email_ticket_login.png
.. _The end-user requests their fellow account information: ../_static/bpmn/fellow_account_retrieval.png
.. _The end-user requests a password reset: ../_static/bpmn/password_reset.png
.. _The end-user registers a new account: ../_static/bpmn/user_registration.png
