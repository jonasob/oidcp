extensions = ['sphinx.ext.autodoc']

templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

project = 'oidcp'

copyright = '2017, ams-hackers'

author = 'ams-hackers'

version = '0.0.1'

release = '0.0.1'

exclude_patterns = ['_build']

pygments_style = 'sphinx'

todo_include_todos = True

html_theme = 'alabaster'

html_theme_options = {
    'page_width': '1280px',
    'sidebar_width': '300px',
}

html_static_path = ['_static']

htmlhelp_basename = 'oidcpdoc'
