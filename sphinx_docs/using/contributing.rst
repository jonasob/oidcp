Contribution Guide
------------------

Get a Local Copy
================

You can find the `source code repository`_ hosted on `Gitlab`_. Please
go ahead and `fork it`_. You'll need to have `Python 3.5.3` or later
installed to build the project.

.. _Gitlab: http://gitlab.com/
.. _source code repository: https://gitlab.com/lukewm/oidcp/
.. _fork it: https://gitlab.com/lukewm/oidcp/forks/new
.. _Python 3.5.3: https://www.python.org/downloads/release/python-353/

Then, clone your local copy using `Git`_:

.. _Git: https://git-scm.com/

.. code:: bash

    $ git clone git@gitlab.com:<your-username>/oidcp.git

Install the System Dependencies
===============================

In order to satisfy the system requirements, please run:

.. code:: bash

    $ make system_install

Please check the packages listed in the `system-requirements.txt`_ first. They
are installed using the `apt`_ command line interface. These requirements have
been tested on `Ubuntu 16.06`_.

.. _system-requirements.txt: https://gitlab.com/lukewm/oidcp/blob/master/system-requirements.txt
.. _apt: TODO
.. _Ubuntu 16.06: TODO

Install the Python Dependencies
===============================

We use `pip`_ and `GNU make`_ to manage our python dependencies.

.. _pip: https://pip.pypa.io/en/stable/
.. _GNU make: https://www.gnu.org/software/make/

You'll need to make sure you have an up-to-date version of `pip`_.

Please upgrade with:

.. code:: bash

    $ pip install -U pip

It is recommended that you install the dependencies in `a virtual
environment`_. You can do all this by running the following:

.. _a virtual environment:  http://docs.python-guide.org/en/latest/dev/virtualenvs/

.. code:: bash

    $ python -m venv .venv  # create venv
    $ . .venv/bin/activate  # activate it
    $ make install          # install dependencies

Running ``make install`` will install all necessary dependencies that you might
need for ``oidcp`` development. If you'd just like to install the minimum
dependencies, please run:

.. code:: bash

    $ pip install -r requirements/base.txt

The project practices `dependency pinning`_ and manages all dependencies in
isolated ``.txt`` files. We do this to minimize the possibility of a single
dependency breaking the software. You can see further examples of this in
practice, in our `continuous integration configuration file`_.

.. _dependency pinning: http://nvie.com/posts/pin-your-packages/
.. _continuous integration configuration file: https://gitlab.com/lukewm/oidcp/blob/master/.gitlab-ci.yml

Configure the Environment
=========================

The application follows the `Twelve-Factor App`_ pattern of determining
configuration from the environment. You'll need to inject the necessary
configuration for your local copy. The variables you'll need are documented in
`the .gitlab-ci.yml`_ file, under the variables field.

.. _Twelve-Factor App: https://12factor.net/config
.. _the .gitlab-ci.yml: https://gitlab.com/lukewm/oidcp/blob/master/.gitlab-ci.yml

Run the Tests
=============

We use `pytest`_ to write our tests. You can run all the tests with:

.. _pytest: http://docs.pytest.org/en/latest/

.. code:: bash

    $ make test

Please refer to the `pytest usage documentation`_ for all the various
command line flags and options that make testing a better experience.

.. _pytest usage documentation: http://doc.pytest.org/en/latest/usage.html

Check Code Quality
==================

We use both `isort`_ and `pylama`_ for code quality assurance.

.. _isort: https://github.com/timothycrosley/isort
.. _pylama: https://github.com/klen/pylama

You can run these both using their Makefile targets:

.. code:: bash

    $ make isort
    $ make lint

Both source and tests are checked for quality. We run a tight ship.

Submit a Change
===============

We are always happy to receive new contributors. If you feel the change would
be a substantial change, please do raise a query on our `issue tracker`_.

.. _issue tracker: https://gitlab.com/lukewm/oidcp/issues

Once you've finished your work, please submit `a merge request`_. If you've
created an issue to discuss this change, please do link to that issue within.

.. _a merge request: https://gitlab.com/lukewm/oidcp/merge_requests

Once your merge request is submitted, the Gitlab continuous integration will
validate your changes. Please view `the configuration file`_ to get familiar
with what is tested.

.. _the configuration file: https://gitlab.com/lukewm/oidcp/blob/master/.gitlab-ci.yml


We provide a convenience Makefile target, to help you locally validate your
change:

.. code:: bash

    $ make ci

Once a core developer has reviewed agreed with your changes, it will be merged.

Gitlab Continuous Integration
=============================

We take advantage of the `Gitlab CI`_ infrastructure in order to automatically
test all changes that are submitted to the source repository. We also use Gitlab
CI as part of our deployment process.

.. _Gitlab CI: https://docs.gitlab.com/ce/ci/README.html

You'll notice in `our configuration file`_ that we use the following:

.. _our configuration file: https://gitlab.com/lukewm/oidcp/blob/master/.gitlab-ci.yml

.. code:: yaml

    image:decentral1se/oidcp-base:latest

This is the `Docker`_ image which has all the system dependencies already
installed, in order to make our continuous integration builds run faster. If
any of your change introduces a new system dependency, you'll need to update
the Dockerfile over at the `oidcp-base`_ source code repository.

.. _Docker: https://www.docker.com/
.. _oidcp-base: https://gitlab.com/lukewm/oidcp-base

Please refer to the `YAML documentation`_ for more information
about syntax and useful configuration options.

.. _YAML documentation: https://docs.gitlab.com/ce/ci/yaml/

Update the Sphinx Documentation
===============================

TODO

Update the Swagger Documentation
================================

TODO

Stay Up to Date
===============

You can stay up to date with current development by 'watching' the source code
repository on Gitlab. You can also review `the change log`_ to see what has happened
betweened versions.

.. _the change log: https://gitlab.com/lukewm/oidcp/blob/master/CHANGELOG.md

Report a Bug
============

If you've found a bug, please don't hesitate to report it on `the issue
tracker`_. If you believe it may compromise the security of our users, please
report confidentially to `ams-security@fsfe.org`_.

.. _the issue tracker: https://gitlab.com/lukewm/oidcp/issues
.. _ams-security@fsfe.org: mailto:ams-security@fsfe.org

Talk to the Team
================

If you'd like to talk to the maintainers, please send an email to `our mailing
list`_, visit `our wiki`_ or report your query on `the issue tracker`_. Feel
free to send usage questions, improvements, feature requests or just say hello!
We're always happy to receive feedback.

.. _our mailing list: ams-hackers@q.fsfe.org
.. _the issue tracker: https://gitlab.com/lukewm/oidcp/issues
.. _our wiki: https://wiki.fsfe.org/Teams/Ams-Hackers
