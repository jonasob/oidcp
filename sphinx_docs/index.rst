AMS OpenID Connect Provider
---------------------------

Welcome to the documentation for FSFE OpenId Connect provider. It's a `Python
3.5.X`_ project built specifically to provide authentication management for the
Account Management System in use by the `Free Software Foundation Europe`_.

This is `Free Software`_, licensed under `the AGPL`_, and we therefore
encourage anyone to apply their four freedoms and use, study, share or modify
the program. If you are not familiar with `Free Software Foundation Europe`_,
please visit `the about page`_ to learn more.

.. _Free Software Foundation Europe: http://fsfe.org/
.. _Free Software: https://www.gnu.org/philosophy/free-sw.en.html
.. _the AGPL: https://www.gnu.org/licenses/agpl-3.0.en.html
.. _the about page: https://fsfe.org/about/about.en.html
.. _more supporters: https://fsfe.org/fellowship/ams/join.php?ams=join
.. _Python 3.5.X: https://www.python.org/downloads/release/python-353/


.. Note::
  ``oidcp`` is still under active development, and as such, there is no way
  to use the software yet. Hold tight! We're working on it.


.. toctree::

    understand/provider
    using/contributing
    maintain/upgrade
