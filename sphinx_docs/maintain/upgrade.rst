Maintanence Guide
-----------------

Upgrade Python Dependencies
===========================

We use `pip`_, `pip-tools`_ and `GNU Make`_ to manage our Python dependencies.

.. _pip: https://pip.pypa.io/en/stable/
.. _pip-tools: https://github.com/jazzband/pip-tools
.. _GNU make: https://www.gnu.org/software/make/

You'll first need to `install pip`_ and then run:

.. _install pip: https://pip.pypa.io/en/stable/installing/

.. code:: bash

    $ pip install -r requirements/administration.txt

Now you have ``pip-tools`` installed, simply run:

.. code:: bash

    $ make upgrade

This will have ``pip-tools`` look into each ``requirements/*.in`` file, choose the
latest version of each dependency and of those, their own dependencies as well.
It will then output `a pinned`_ list in a corresponding ``requirements/.txt`` file.

.. _a pinned: http://nvie.com/posts/pin-your-packages/

You can run a ``git diff`` to see what updates are available. If you're happy
with the upgrade, commit the changes in a branch and submit a merge request
which can then be validated via the Gitlab CI run.

Typically, an upgrade with a few major upgrade releases should be carefully
watched. Please include any Changelog entries in your merge request from
releases that you think may be problematic.


Check if all the provider services are up
=========================================

In order for the provider to function properly, there are a number of services
such as an LDAP, Redis, mail server and a celery worker that should be running
and have working connections to the provider.

In order to debug if those are available in the production environment, the `check`_
script is provided for convenience. You can check usage details by simply running:

.. code:: bash

    $ ./check

.. _check: https://gitlab.com/lukewm/oidcp/blob/master/check
