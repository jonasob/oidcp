"""Bootstrap the application for Celery."""

import os

from oidcp.app import celery, create_app  # noqa
from oidcp.config import Prod, Test
from distutils.util import strtobool

if strtobool(os.environ.get('DEBUG', False)):
    app = create_app(config=Test)
else:
    app = create_app(config=Prod)
app.app_context().push()
