# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Introspection endpoint testing."""

from base64 import b64encode
from datetime import datetime as dt
from datetime import timedelta

from flask import url_for
from freezegun import freeze_time
from werkzeug.datastructures import Headers


def test_introspect(client, token_params, session_with_record, auth_header):
    url = url_for("token")
    token_params.update({"code": session_with_record['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    url = url_for("introspect")
    data = dict(access_token=resp.json['access_token'])

    resp = client.post(url, data=data, headers=auth_header)
    assert resp.status_code == 200
    assert resp.json['active'] is True


def test_introspect_bad_payload(client, session, token_params, auth_header):
    url = url_for("introspect")
    data = dict(access_token="INVALID")
    resp = client.post(url, data=data, headers=auth_header)
    assert resp.status_code == 200
    assert resp.json['active'] is False

    resp = client.post(url, data={}, headers=auth_header)
    assert resp.status_code == 422
    assert "access_token" in resp.json['errors']


def test_introspect_bad_auth(client, token_params, session_with_record):
    url = url_for("token")
    token_params.update({'code': session_with_record['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    url = url_for("introspect")
    token = resp.json['access_token']
    data = dict(access_token=token)
    encoded = b64encode(bytes("IN:VALID", "utf-8")).decode("ascii")
    bad_header = Headers({'Basic': encoded})

    resp = client.post(url, data=data, headers=bad_header)
    assert resp.status_code == 401


def test_introspect_token_expired(app, client, token_params,
                                  session_with_record, auth_header):
    url = url_for("token")
    token_params.update({'code': session_with_record['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    url = url_for("introspect")
    data = dict(access_token=resp.json['access_token'])

    expired_token = dt.now() + timedelta(days=100)
    with freeze_time(expired_token):
        resp = client.post(url, data=data, headers=auth_header)
        assert resp.status_code == 200
        assert resp.json['active'] is False


def test_introspect_token_revoked(app, client, token_params,
                                  session_with_record, auth_header):
    url = url_for("token")
    token_params.update({'code': session_with_record['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    app.provider.sdb.revoke_token(session_with_record['code'])

    url = url_for("introspect")
    data = dict(access_token=resp.json['access_token'])

    resp = client.post(url, data=data, headers=auth_header)
    assert resp.status_code == 200
    assert resp.json['active'] is False


def test_introspect_bad_http_method(client):
    url = url_for("introspect")
    resp = client.get(url)
    assert resp.status_code == 405
