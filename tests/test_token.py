# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Token endpoint testing."""

from datetime import datetime as dt
from datetime import timedelta

import jwt
from flask import url_for
from freezegun import freeze_time


def test_token(app, client, token_params,
               session_with_record, auth_params):
    token_params.update({'code': session_with_record['code']})
    url = url_for("token")

    assert not session_with_record['code_used']

    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    actual_keys = resp.json.keys()
    expected_keys = [
        "access_token", "id_token",
        "token_type", "scope", "state"
    ]
    assert set(actual_keys) == set(expected_keys)

    assert resp.json['scope'] == "openid"
    assert resp.json['token_type'] == "Bearer"
    assert resp.json['state'] == "some_state"

    client_id = app.config['FRONTEND_ID']
    secret = app.config['CLIENT'][client_id]['client_secret']
    alg = app.config['CLIENT'][client_id]['id_token_signed_response_alg']

    token = resp.json['id_token']
    decoded = jwt.decode(
        token, secret, algorithms=alg,
        audience=app.config['FRONTEND_ID']
    )

    assert decoded['iss'] == app.config['ISSUER']
    assert decoded['nonce'] == auth_params['nonce']
    assert decoded['aud'] == [auth_params['client_id']]
    assert decoded['record'] == session_with_record['record']

    sid = app.provider.sdb.uid2sid['alice']
    assert len(sid) == 1
    session = app.provider.sdb[sid[0]]
    assert session['code_used']


def test_token_missing_code(client, token_params):
    url = url_for("token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 422
    assert "code" in resp.json['errors']


def test_token_missing_record(client, token_params, session):
    token_params.update({'code': session['code']})
    url = url_for("token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 422
    assert "record" in resp.json['errors']


def test_token_code_reuse(client, token_params, session_with_record):
    token_params.update({'code': session_with_record['code']})
    url = url_for("token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    resp = client.post(url, data=token_params)
    assert resp.status_code == 400
    assert "oic" in resp.json['errors']


def test_token_invalid_code(client, token_params):
    token_params.update({'code': 'INVALID'})
    url = url_for("token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 422
    assert "code" in resp.json['errors']


def test_token_code_timeout(app, client, session_with_record,
                            token_params):
    token_params.update({'code': session_with_record['code']})
    url = url_for("token")

    expiration_time = dt.now() + timedelta(minutes=30)
    with freeze_time(expiration_time):
        resp = client.post(url, data=token_params)
        assert resp.status_code == 422
        assert "code" in resp.json['errors']


def test_token_bad_http_method(client):
    url = url_for("token")
    resp = client.get(url)
    assert resp.status_code == 405
