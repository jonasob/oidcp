# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""View functionality testing."""

import pytest
from flask import Response as FlaskResponse

from oic.utils.http_util import Response as PyOIDCResponse
from oidcp.exceptions import MissingImplementation
from oidcp.views import CustomResource


def test_custom_resource_handler(client, auth_params):
    resource_class = CustomResource()

    flask_response = client.get("/")
    handled = resource_class.handler(flask_response)
    assert isinstance(handled, FlaskResponse)

    pyoidc_response = PyOIDCResponse(message="foo", status="200 OK")
    handled = resource_class.handler(pyoidc_response)
    assert isinstance(handled, FlaskResponse)
    assert handled.status_code == 200
    assert str(handled.data, "utf-8") == "foo"

    pyoidc_response = PyOIDCResponse(message="foo", status="200 OK")
    pyoidc_response.headers = [("Content-type", "unknown/type")]
    with pytest.raises(MissingImplementation):
        resource_class.handler(pyoidc_response)
