# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Pytest fixtures for testing."""

from base64 import b64encode

import pytest
from werkzeug.datastructures import Headers

from oidcp.app import create_app
from oidcp.config import Test


@pytest.fixture
def ldap_mock(app):
    """A mocked LDAP connection."""
    conn = app.config['LDAP_CONN']
    yield conn.start()
    conn.stop()


@pytest.fixture
def app():
    """A testing application."""
    _app = create_app(Test)
    ctx = _app.test_request_context()
    ctx.push()
    yield _app
    _app.provider.sdb._db.flushall()
    ctx.pop()


@pytest.fixture
def auth_params(app):
    """All mandatory parameters for an authentication request."""
    return dict(
        response_type=app.config['FLOW'],
        client_id=app.config['FRONTEND_ID'],
        redirect_uri=app.config['FRONTEND_URL'],
        scope=app.config['SCOPE'],
        state='some_state',
        nonce='some_nonce'
    )


@pytest.fixture
def token_params(app):
    """All mandatory parameters for an ID token request."""
    client_id = app.config['FRONTEND_ID']
    secret = app.config['CLIENT'][client_id]['client_secret']
    return dict(
        grant_type='authorization_code',
        redirect_uri=app.config['FRONTEND_URL'],
        client_id=app.config['FRONTEND_ID'],
        client_secret=secret,
        state='some_state'
    )


@pytest.fixture
def session(app, auth_params):
    """A PyOIDC generated session."""
    auth_params.update(dict(login_type="username"))
    session_id = app.provider.create_session(auth_params, "alice")
    session = app.provider.sdb[session_id]
    yield session
    del app.provider.sdb[session_id]


@pytest.fixture
def session_with_record(app, auth_params):
    """A PyOIDC generated session with record identifier."""
    session_db = app.provider.sdb

    auth_params.update(dict(login_type="username"))
    session_id = app.provider.create_session(auth_params, "alice")

    session = session_db[session_id]
    session['record'] = "some_record_id"
    session_db[session_id] = session

    yield session
    del app.provider.sdb[session_id]


@pytest.fixture
def auth_header(app):
    """Basic authentication header for protected end-points."""
    client_id = app.config['BACKCHANNEL_ID']
    secret = app.config['BACKCHANNEL_PWORD']
    unencoded = "{}:{}".format(client_id, secret)
    encoded = b64encode(bytes(unencoded, "utf-8")).decode("ascii")
    return Headers({'Basic': encoded})
