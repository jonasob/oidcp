# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Authorization endpoint testing."""

from urllib.parse import urlencode

from flask import url_for
from ldap import NO_SUCH_OBJECT
from requests import Timeout


def test_auth_login_type(client, auth_params):
    url = url_for("auth")

    resp = client.get(url, query_string=urlencode(auth_params))
    assert resp.status_code == 422
    assert "login_type" in resp.json['errors']

    auth_params.update(login_type="INVALID")
    resp = client.get(url, query_string=urlencode(auth_params))
    assert resp.status_code == 422
    assert "login_type" in resp.json['errors']

    auth_params.update(login_type="username")
    resp = client.get(url, query_string=urlencode(auth_params))
    assert resp.status_code == 200
    assert url_for("auth", _external=True) in str(resp.data)

    auth_params.update(login_type="email")
    resp = client.get(url, query_string=urlencode(auth_params))
    assert resp.status_code == 200
    assert url_for("auth", _external=True) in str(resp.data)


def test_auth_username(app, client, auth_params, ldap_mock, mocker):
    redis = app.provider.sdb._db
    assert len(redis.keys()) == 0

    url = url_for('auth')
    auth_params.update(login_type='username')
    query = urlencode(auth_params)
    payload = dict(username='alice', password='passw0rd', query=query)

    mocked = mocker.Mock()
    mocked.status_code = 200
    mocked_record_id = 'foobar'
    mocked.json = {'alice@fsfe.org': mocked_record_id}
    target = 'oidcp.tasks.requests.get'

    with mocker.patch(target, return_value=mocked):
        resp = client.post(url, data=payload)

    assert resp.status_code == 303
    assert "code=" in str(resp.data)
    assert auth_params['state'] in str(resp.data)

    assert len(redis.keys()) == 1

    session_id = str(redis.keys()[0], 'utf-8')
    session = redis[session_id]
    assert session['code_used'] is False
    assert session['revoked'] is False
    assert session['client_id'] == app.config['FRONTEND_ID']
    assert session['nonce'] == auth_params['nonce']
    assert session['state'] == auth_params['state']
    assert session['record'] == mocked_record_id


def test_auth_username_bad_credentials(client, auth_params, ldap_mock):
    url = url_for('auth')
    auth_params.update(login_type='username')
    query = urlencode(auth_params)

    payload = dict(username='alice', password='INVALID', query=query)
    resp = client.post(url, data=payload)
    assert resp.status_code == 401

    payload = dict(username='INVALID', password='passw0rd', query=query)
    resp = client.post(url, data=payload)
    assert resp.status_code == 401


def test_auth_username_bad_payload(client, auth_params):
    url = url_for("auth")
    auth_params.update(login_type="username")
    payload = dict(username="alice", password="passw0rd")
    resp = client.post(url, data=payload)
    assert resp.status_code == 422
    assert "query" in resp.json['errors']


def test_auth_username_cant_match_email_ldap(app, client, auth_params,
                                             ldap_mock, mocker, caplog):
    redis = app.provider.sdb._db
    assert len(redis.keys()) == 0

    url = url_for('auth')
    auth_params.update(login_type='username')
    query = urlencode(auth_params)
    payload = dict(username='alice', password='passw0rd', query=query)

    target = 'mockldap.ldapobject.LDAPObject.search_s'
    with mocker.patch(target, return_value=NO_SUCH_OBJECT):
        resp = client.post(url, data=payload)

    assert resp.status_code == 303
    assert "code=" in str(resp.data)
    assert auth_params['state'] in str(resp.data)

    assert len(redis.keys()) == 1

    session_id = str(redis.keys()[0], 'utf-8')
    session = redis[session_id]
    assert "record" not in session.keys()

    msg = "unable to retrieve LDAP account email"
    assert any([msg in record.msg for record in caplog.records()])


def test_auth_email(app, client, auth_params, ldap_mock, mocker):
    redis = app.provider.sdb._db
    assert len(redis.keys()) == 0

    url = url_for('auth')
    auth_params.update(login_type='email')
    query = urlencode(auth_params)
    payload = dict(email='alice@fsfe.org', query=query)

    mocked = mocker.Mock()
    mocked.status_code = 200
    mocked_record_id = 'foobar'
    mocked.json = {'alice@fsfe.org': mocked_record_id}
    target = 'oidcp.tasks.requests.get'

    with app.mail.record_messages() as outbox:
        assert len(outbox) == 0

        with mocker.patch(target, return_value=mocked):
            response = client.post(url, data=payload)

        assert response.status_code == 202
        assert len(outbox) == 1
        assert auth_params['redirect_uri'] in outbox[0].body

    assert len(redis.keys()) == 1

    session_key = str(redis.keys()[0], 'utf-8')
    session = redis[session_key]
    assert session['code_used'] is False
    assert session['revoked'] is False
    assert session['client_id'] == app.config['FRONTEND_ID']
    assert session['nonce'] == auth_params['nonce']
    assert session['state'] == auth_params['state']
    assert session['record'] == mocked_record_id


def test_auth_email_bad_payload(client, auth_params):
    url = url_for("auth")
    auth_params.update(login_type="email")
    query = urlencode(auth_params)
    payload = dict(query=query)
    resp = client.post(url, data=payload)
    assert resp.status_code == 422
    assert "login_type" in resp.json['errors']


def test_auth_email_missing_account(client, auth_params, ldap_mock):
    url = url_for('auth')
    auth_params.update(login_type='email')
    query = urlencode(auth_params)
    payload = dict(email='NOT@THERE.COM', query=query)
    resp = client.post(url, data=payload)
    assert resp.status_code == 422
    assert "email" in resp.json['errors']


def test_auth_email_cannot_retrieve_record(app, client, auth_params,
                                           ldap_mock, mocker, caplog):
    redis = app.provider.sdb._db
    assert len(redis.keys()) == 0

    url = url_for('auth')
    auth_params.update(login_type='email')
    query = urlencode(auth_params)
    payload = dict(email='alice@fsfe.org', query=query)

    target = 'oidcp.tasks.requests.get'
    with mocker.patch(target, side_effect=Timeout()):
        response = client.post(url, data=payload)
    assert response.status_code == 202

    assert len(redis.keys()) == 1

    session_key = str(redis.keys()[0], 'utf-8')
    session = redis[session_key]
    assert "record" not in session.keys()

    msg = "unable to retrieve record identifier"
    assert any([msg in record.msg for record in caplog.records()])
