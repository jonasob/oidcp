# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Logout endpoint testing."""

from flask import url_for


def test_logout(app, client, session_with_record, token_params):

    url = url_for("token")
    token_params.update({'code': session_with_record['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    redis = app.provider.sdb._db
    assert len(redis.keys()) == 1

    url = url_for("logout")
    data = dict(access_token=resp.json['access_token'])
    resp = client.post(url, data=data)
    assert resp.status_code == 200

    assert len(redis.keys()) == 0


def test_logout_bad_params(app, client):
    url = url_for("logout")
    data = dict(invalid="params")
    resp = client.post(url, data=data)
    assert resp.status_code == 422
    assert "access_token" in resp.json['errors']


def test_logout_invalid_token(app, client, session_with_record, token_params):
    url = url_for("token")
    token_params.update({'code': session_with_record['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    redis = app.provider.sdb._db
    assert len(redis.keys()) == 1

    url = url_for("logout")
    data = dict(access_token="INVALID")
    resp = client.post(url, data=data)
    assert resp.status_code == 422
    assert "access_token" in resp.json['errors']

    assert len(redis.keys()) == 1
